import pymongo


class DbUtils:
    def __init__(self):
        client = pymongo.MongoClient('mongodb://mongodb:27017/')
        db = client['katalon_bot_db']
        self.users = db['users']

    def subscribe(self, user_id, suite):
        text = 'Got it! You\'re subscribed to `{}`'.format(suite)
        suite = suite.lower()
        try:
            user = self.get_user_from_db(user_id)
        except ValueError as e:
            print(e.args[0], flush=True)
            user = {'user_id': user_id, 'suites': [suite]}
            self.users.insert_one(user)
        else:
            suites = user['suites']
            if suite in suites:
                text = 'You\'re already subscribed!'
            else:
                suites.append(suite)
                self.update_user(user_id, suites)
        finally:
            return text

    def unsubscribe(self, user_id, suite):
        text = 'Got it! You\'re unsubscribed from `{}`'.format(suite)
        suite = suite.lower()
        try:
            user = self.get_user_from_db(user_id)
        except ValueError as e:
            print(e.args[0], flush=True)
            text = 'You don\'t have any subscriptions.'
        else:
            suites = user['suites']
            if suite == 'all':
                suites.clear()
                self.update_user(user_id, suites)
            elif suite in suites:
                suites.remove(suite)
                self.update_user(user_id, suites)
            else:
                text = 'You\'re already unsubscribed from `{}`'.format(suite)
        finally:
            return text

    def list_subs(self, user_id):
        text = 'You have 0 subscriptions'
        try:
            user = self.get_user_from_db(user_id)
        except ValueError as e:
            print(e.args[0], flush=True)
        else:
            suites = user['suites']
            if len(suites) > 0:
                text = 'Here are your current subscriptions:\n'
                text += '```'
                for suite in suites:
                    text += suite + '\n'
                text += '```'
        finally:
            return text

    def get_user_from_db(self, user_id):
        user = self.users.find_one({'user_id': user_id})
        if user:
            return user
        else:
            raise ValueError('User not found in db.')

    def update_user(self, user_id, suites):
        self.users.update_one({'user_id': user_id}, {'$set': {'suites': suites}})

    def get_users_subscribed_to_suite(self, suite):
        subscribed_users = []
        for user in self.users.find({'suites': suite.lower()}):
            subscribed_users.append(user['user_id'])
        return subscribed_users
