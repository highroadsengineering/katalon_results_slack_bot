# katalon_results_slack_bot

The purpose of this slack bot is to monitor the #katalon_results slack channel and
pass the automation results posted there to individual team members via DM. Users
can interact with the bot via mentions in the #katalon_results channel. The 
following commands are currently supported:

```
sub <suite name>   ... subscribe to results for the suite
unsub <suite name> ... unsubscribe from results for the suite
unsub all          ... unsubscribe from all results
list subs          ... list all current subscriptions
help               ... show help
```

For example, if you see the following katalon results message posted in the
channel:
```
Summary execution result of test suite: Test Suites/BLDINT/Sanity/BCBSMA-Plans
Total test cases: 6
Total passes: 1
Total failures: 1
Total errors: 4
Total skipped: 0
```
Post the message `@Katalon Results Bot sub BCBSMA-Plans` in the channel to
receive all future results for the BCBSMA-Plans suite in a DM. User subscriptions
are stored in a mongo database.

---

# Running the bot

The following environment variables are required:
```
SLACK_SOCKET_TOKEN  ... App-level token
KATALON_BOT_TOKEN   ... Bot user OAuth token
REPORTS_CHANNEL     ... ID of the #katalon_results channel
KATALON_RESULTS_BOT ... ID of the HRAF-KatalonResults app/user
```
The bot can be started through docker compose:
```
docker-compose -f katalon_results_bot.yml up -d
```