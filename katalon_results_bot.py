from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
from decouple import config
from dbutils import DbUtils

app = App(token=config('KATALON_BOT_TOKEN'))
dbutils = DbUtils()


# Function for processing mentions of the slackbot. Supported functions via mention are:
# sub <suite name> - subscribes user to the suite
# unsub <suite name> - unsubscribe user from the suite
# unsub all - unsubscribe user from all suites
# list subs - list user's currently subscribed suites
# help - displays a help message
@app.event('app_mention')
def app_mention(body):
    print('Mention detected...', flush=True)
    channel = get_channel(body)
    thread_id = get_thread_id(body)
    message_text = get_message_text(body)
    message_parts = message_text.split()
    user_id = get_user_id(body)
    try:
        check_message(message_parts)
        print('Processing message: {} from user {}'.format(message_text, user_id), flush=True)
        if message_parts[1] == 'sub':
            print('Subscribing...', flush=True)
            text = dbutils.subscribe(user_id, message_parts[2])
            post_message(channel, text, thread_id)
        elif message_parts[1] == 'unsub':
            print('Unsubscribing...', flush=True)
            text = dbutils.unsubscribe(user_id, message_parts[2])
            post_message(channel, text, thread_id)
        elif message_parts[1] == 'list':
            print('Listing subscriptions...', flush=True)
            text = dbutils.list_subs(user_id)
            post_message(channel, text, thread_id)
        else:
            print('Showing help...', flush=True)
            show_help(channel, thread_id)

    except ValueError as e:
        print(e.args[0], flush=True)


# Processes messages posted in channels the bot is a member of. It first checks to see if the message is
# in the expected channel (REPORTS_CHANNEL env variable) and from the expected user (KATALON_RESULTS_BOT env variable).
# If not, the message is ignored. Otherwise the suite name is retrieved and the message is passed on via DM to
# all subscribed users.
@app.event('message')
def channel_message(body):
    channel = get_channel(body)
    user_id = get_user_id(body)
    thread_id = get_thread_id(body)
    print('Message received in channel {} from user {}'.format(channel, user_id), flush=True)
    try:
        check_channel(channel)
        check_user_id(user_id)
        message_text = get_message_text(body)
        suite_name = get_suite_name(message_text)
        print('Suite name: {}'.format(suite_name), flush=True)
        subscribed_users = dbutils.get_users_subscribed_to_suite(suite_name)
        print('Found {} subscribed users'.format(len(subscribed_users)), flush=True)
        for subscribed_user in subscribed_users:
            post_message(subscribed_user, message_text)

    except ValueError as e:
        print(e.args[0], flush=True)


# Checks to see if the given channel id matches the REPORTS_CHANNEL env variable
def check_channel(channel):
    if channel != config('REPORTS_CHANNEL'):
        raise ValueError('Wrong channel, ignoring.')


# Checks to see if the given user id matches the KATALON_RESULTS_BOT env variable
def check_user_id(user_id):
    if user_id != config('KATALON_RESULTS_BOT'):
        raise ValueError('Wrong user, ignoring.')


# Verifies that the message has the appropriate number of arguments
def check_message(message):
    if len(message) < 1 or len(message) > 3:
        raise ValueError('Wrong number of arguments.')


# Retrieves the message text from the slack event body
def get_message_text(body):
    return body['event']['text']


# Retrieves the user id from the slack event body
def get_user_id(body):
    try:
        return body['event']['user']
    except KeyError as e:
        return body['event']['bot_id']


# Retrieves the channel id from the slack event body
def get_channel(body):
    return body['event']['channel']


# Retrieves the thread id from the slack event body
def get_thread_id(body):
    return body['event']['ts']


# Extracts the suite name from the report posted in the channel
def get_suite_name(message):
    return message.split('/')[-1].split()[0]


# Builds and displays the help message
def show_help(channel, thread_id):
    text = 'Hey there! I\'m a bot that sends these katalon results on to specific users. Here\'s what you can do:\n'
    text += '`sub <suite name>`: For example enter `sub CEPSanity` to receive a DM with the CEPSanity results\n'
    text += '`unsub <suite name>`: Stop receiving DMs for the suite\n'
    text += '`unsub all`: Stop receiving DMs for all subscribed suites\n'
    text += '`list subs`: List all suites you are currently subscibed to\n'
    text += '`help`: Display this help message'
    post_message(channel, text, thread_id)


# Posts the given message to the given channel
def post_message(channel, text, thread_id='0'):
    app.client.chat_postMessage(channel=channel, text=text, thread_ts=thread_id)


if __name__ == "__main__":
    SocketModeHandler(app, config('SLACK_SOCKET_TOKEN')).start()
