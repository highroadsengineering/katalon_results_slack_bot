FROM python:3.10.0-slim
RUN useradd --create-home --shell /bin/bash bot_user
WORKDIR /home/bot_user
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000 27017
USER bot_user
COPY . .
CMD ["python", "katalon_results_bot.py"]
